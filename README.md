# DevOps Bootcamp
Hi! Welcome to DevOps Bootcamp. We hope you enjoy the DevOps learning journey, It is quite a pipeline!

The structure of our DevOps bootcamp is designed to provide a comprehensive and hands-on learning experience. Throughout the program, you will engage in a dynamic blend of theoretical learning and practical execution, allowing you to internalize theoretical concepts and immediately apply them in real-world scenarios. 

This project contains two applications and a SQL Database:
 1. Stationery.Api - .Net 6 WebApi
 2. Stationery.UI - React App
 3. Sql Database will be run with a docker command: `docker run -d --restart always --name sqldatabase -p 1435:1433 deemajor/fsadb`

# Lesson 1: Project Set Up & Powershell

**Outcome:**

- Understand dependencies and set up the Windows PATH
- Set up a repository for a project
- Build projects locally
- Use Powershell for automation